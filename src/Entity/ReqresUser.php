<?php

namespace Drupal\reqres_users\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\reqres_users\ReqresUserInterface;

/**
 * Defines the reqres user entity class.
 *
 * @ContentEntityType(
 *   id = "reqres_user",
 *   label = @Translation("Reqres User"),
 *   label_collection = @Translation("Reqres Users"),
 *   label_singular = @Translation("reqres user"),
 *   label_plural = @Translation("reqres users"),
 *   label_count = @PluralTranslation(
 *     singular = "@count reqres users",
 *     plural = "@count reqres users",
 *   ),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\reqres_users\ReqresUserAccessControlHandler",
 *   },
 *   base_table = "reqres_user",
 *   admin_permission = "administer reqres user",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/reqres-user",
 *     "add-form" = "/reqres-user/add",
 *     "canonical" = "/reqres-user/{reqres_user}",
 *     "edit-form" = "/reqres-user/{reqres_user}",
 *     "delete-form" = "/reqres-user/{reqres_user}/delete",
 *   },
 * )
 */
class ReqresUser extends ContentEntityBase implements ReqresUserInterface {

  public static function createOrUpdate(int $id, array $values = []): ReqresUser {
    $reqresUser = self::load($id) ?? self::create(['id' => $id]);

    foreach ($values as $key => $value) {
      $reqresUser->set($key, $value);
    }

    $reqresUser->save();
    return $reqresUser;
  }

  public function getFirstName(): string {
    return $this->get('first_name')->getString();
  }

  public function getLastName(): string {
    return $this->get('last_name')->getString();
  }

  public function getEmailAddress(): string {
    return $this->get('email')->getString();
  }

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['email'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Email'))
      ->setDescription(t('The email of this user.'))
      ->setDefaultValue('Unknown')
      ->setDisplayOptions('form', [
        'type' => 'hidden',
      ]);

    $fields['first_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('First Name'))
      ->setDescription(t('The first name of the Reqres user.'))
      ->setDefaultValue('Unknown')
      ->setDisplayOptions('form', [
        'type' => 'hidden',
      ]);

    $fields['last_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Last Name'))
      ->setDescription(t('The last name of the Reqres user.'))
      ->setDefaultValue('Unknown')
      ->setDisplayOptions('form', [
        'type' => 'hidden',
      ]);

    return $fields;
  }
}

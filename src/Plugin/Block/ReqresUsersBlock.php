<?php

namespace Drupal\reqres_users\Plugin\Block;

use Drupal\Component\Utility\Html;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\reqres_users\Entity\ReqresUser;
use Drupal\reqres_users\Event\ReqresUserQueryAlter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Provides block displaying a paginated list of Reqres users.
 *
 * @Block(
 *   id = "reqres_users",
 *   admin_label = @Translation("Reqres Users List"),
 *   category = @Translation("Reqres Users")
 * )
 */
class ReqresUsersBlock extends BlockBase implements ContainerFactoryPluginInterface {

  protected EntityStorageInterface $reqresUserStorage;

  protected const DEFAULT_PAGE_LIMIT = 6;

  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager,
    EventDispatcherInterface $eventDispatcher
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->reqresUserStorage = $entityTypeManager->getStorage('reqres_user');
    $this->eventDispatcher = $eventDispatcher;
  }

  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('event_dispatcher')
    );
  }

  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['users_per_page'] = [
      '#type' => 'number',
      '#title' => $this->t('Users Per Page'),
      '#description' => $this->t('The number of users to display on each page.'),
      '#default_value' => $this->configuration['users_per_page'] ?? self::DEFAULT_PAGE_LIMIT,
      '#min' => 1,
      '#max' => 100,
      '#step' => 1,
    ];

    $form['first_name_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('First Name Label'),
      '#description' => $this->t('The label for the first name column header.'),
      '#default_value' => $config['first_name_label'] ?? $this->t('First Name'),
    ];

    $form['last_name_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Last Name Label'),
      '#description' => $this->t('The label for the last name column header.'),
      '#default_value' => $config['last_name_label'] ?? $this->t('Last Name'),
    ];

    $form['email_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email Address Label'),
      '#description' => $this->t('The label for the email address column header.'),
      '#default_value' => $config['email_label'] ?? $this->t('Email Address'),
    ];

    return $form;
  }

  public function blockSubmit($form, \Drupal\Core\Form\FormStateInterface $form_state): void {
    parent::blockSubmit($form, $form_state);

    $this->configuration['users_per_page'] = $form_state->getValue('users_per_page');
    $this->configuration['first_name_label'] = $form_state->getValue('first_name_label');
    $this->configuration['last_name_label'] = $form_state->getValue('last_name_label');
    $this->configuration['email_label'] = $form_state->getValue('email_label');
  }


  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = [];
    $rows = [];
    $users = $this->getUsers();

    foreach ($users as $user) {
      $build['#cache']['tags'][] = 'reqres_user:' . $user->id();
    }

    $header = $this->getHeader();

    /** @var \Drupal\reqres_users\Entity\ReqresUser $user */
    foreach ($users as $user) {
      $rows[] = $this->getRowInfo($user);
    }

    $build['list'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No users found'),
    ];
    $build['pager'] = [
      '#type' => 'pager',
    ];

    return $build;
  }


  /** @return ReqresUser[] */
  protected function getUsers(): array {
    $config = $this->getConfiguration();
    $pageLimit = $config['users_per_page'] ?? self::DEFAULT_PAGE_LIMIT;

    $query = $this->reqresUserStorage
      ->getQuery()
      ->accessCheck()
      ->pager($pageLimit)
      ->sort('id');

    $queryAlterEvent = new ReqresUserQueryAlter($query);

    $this->eventDispatcher->dispatch($queryAlterEvent, ReqresUserQueryAlter::ALTER);

    $ids = $query->execute();

    return $this->reqresUserStorage->loadMultiple($ids);
  }

  protected function getHeader(): array {
    $config = $this->getConfiguration();

    return [
      ['data' => $config['first_name_label'] ?? $this->t('First Name')],
      ['data' => $config['last_name_label'] ?? $this->t('Last Name')],
      ['data' => $config['email_label'] ?? $this->t('Email Address')],
    ];
  }

  protected function getRowInfo(ReqresUser $user): array {
    return [
      'data' => [
        'first_name' => Html::escape($user->getFirstName()),
        'last_name' => Html::escape($user->getLastName()),
        'email' => Html::escape($user->getEmailAddress())
      ],
    ];
  }

  public function getCacheTags() {
    $tags = parent::getCacheTags();
    $tags[] = 'config:block.block.' . $this->getPluginId();

    return $tags;
  }

}

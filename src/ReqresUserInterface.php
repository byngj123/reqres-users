<?php

namespace Drupal\reqres_users;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\reqres_users\Entity\ReqresUser;

/**
 * Provides an interface defining a reqres user entity type.
 */
interface ReqresUserInterface extends ContentEntityInterface {

  public static function createOrUpdate(int $id, array $values = []): ReqresUser;

  public function getFirstName(): string;

  public function getLastName(): string;

  public function getEmailAddress(): string;

}

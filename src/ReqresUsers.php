<?php

namespace Drupal\reqres_users;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\reqres_users\Entity\ReqresUser;
use GuzzleHttp\ClientInterface;

/**
 * Service description.
 */
class ReqresUsers {

  protected ClientInterface $client;

  protected LoggerChannelInterface $logger;

  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a ReqresUsers object.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The HTTP client.
   */
  public function __construct(
    ClientInterface $client,
    LoggerChannelFactoryInterface $loggerChannel,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->client = $client;
    $this->logger = $loggerChannel->get('Reqres User Sync');
    $this->entityTypeManager = $entityTypeManager;
  }

  public function syncUsers(): void {
    try {
      $data = $this->getPagedUserData();

      foreach ($data as $userData) {
        $userId[] = $userData->id;
        $this->createUser($userData);
      }
      $this->removeOldUsers($userId);
    }
    catch (\Exception $exception) {
      $this->logger->error($exception->getMessage());
    }
  }

  protected function getPagedUserData(): array {
    $response = $this->getApiResponse();
    $totalPages = $response->total_pages;
    $data = $response->data ?? [];

    if ($totalPages > 1) {
      for ($page = 2; $page <= $totalPages; $page++) {
        $data = array_merge($data, $this->getApiResponse($page)->data);
      }
    }

    return $data;
  }

  /**
   * @return \stdClass
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function getApiResponse(int $page = 1): \stdClass {
    $response = $this->client->request(
      'get',
      'https://reqres.in/api/users',
      ['query' => ['page' => $page]]
    );

    if ($response->getStatusCode() !== 200) {
      throw new \Exception('Unable to access API. Error code: %code', $response->getStatusCode());
    }

    return json_decode($response->getBody()->getContents());
  }

  protected function createUser(\stdClass $userData): int {
    $id = $userData->id;
    $email = $userData->email;
    $firstName = $userData->first_name;
    $lastName = $userData->last_name;

    $reqresUser = ReqresUser::createOrUpdate(
      $id,
      [
        'email' => $email,
        'first_name' => $firstName,
        'last_name' => $lastName,
      ]
    );

    return $reqresUser->save();
  }

  protected function removeOldUsers(array $userIds) {
    $storage = $this->entityTypeManager->getStorage('reqres_user');

    $ids = $storage->getQuery()
      ->accessCheck()
      ->condition('id', $userIds, 'NOT IN')
      ->execute();

    foreach ($ids as $id) {
      ReqresUser::load($id)->delete();
    }
  }

}

<?php

namespace Drupal\reqres_users\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\Query\QueryInterface;

class ReqresUserQueryAlter extends Event {

  const ALTER = 'reqres_users.query_alter';

  protected QueryInterface $query;

  public function __construct(QueryInterface $query) {
    $this->query = $query;
  }

  public function getQuery(): QueryInterface {
    return $this->query;
  }

}

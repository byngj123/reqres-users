<?php

namespace Drupal\Tests\reqres_users\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\reqres_users\Entity\ReqresUser;

/**
 * Test description.
 *
 * @group reqres_users
 */
class ReqresUserTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['reqres_users'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('reqres_user');
  }

  /** @test */
  public function can_create_new_user() {
    $user = ReqresUser::createOrUpdate(1, [
      'first_name' => 'John',
      'last_name' => 'Doe',
      'email' => 'john.doe@example.com',
    ]);

    $this->assertEquals('John', $user->getFirstName());
    $this->assertEquals('Doe', $user->getLastName());
    $this->assertEquals('john.doe@example.com', $user->getEmailAddress());

  }

  /** @test */
  public function can_update_existing_user() {
    $user = ReqresUser::createOrUpdate(1, [
      'first_name' => 'John',
      'last_name' => 'Doe',
      'email' => 'john.doe@example.com',
    ]);

    $this->assertEquals('John', $user->get('first_name')->getString());
    $this->assertEquals('Doe', $user->get('last_name')->getString());
    $this->assertEquals('john.doe@example.com', $user->get('email')->getString());

    $updatedUser = ReqresUser::createOrUpdate(1, [
      'first_name' => 'Jane',
      'last_name' => 'Doe',
      'email' => 'jane.doe@example.com',
    ]);

    $this->assertEquals('Jane', $updatedUser->get('first_name')->getString());
    $this->assertEquals('Doe', $updatedUser->get('last_name')->getString());
    $this->assertEquals('jane.doe@example.com', $updatedUser->get('email')->getString());
  }

  /** @test */
  public function can_get_first_name() {
    $user = ReqresUser::createOrUpdate(1, [
      'first_name' => 'John',
      'last_name' => 'Doe',
      'email' => 'john.doe@example.com',
    ]);

    // Verify the entity was created with the correct values.
    $this->assertEquals('John', $user->getFirstName());
  }

  /** @test */
  public function can_get_last_name() {
    $user = ReqresUser::createOrUpdate(1, [
      'first_name' => 'John',
      'last_name' => 'Doe',
      'email' => 'john.doe@example.com',
    ]);

    // Verify the entity was created with the correct values.
    $this->assertEquals('Doe', $user->getLastName());
  }

  /** @test */
  public function can_get_email_address() {
    $user = ReqresUser::createOrUpdate(1, [
      'first_name' => 'John',
      'last_name' => 'Doe',
      'email' => 'john.doe@example.com',
    ]);

    // Verify the entity was created with the correct values.
    $this->assertEquals('john.doe@example.com', $user->getEmailAddress());
  }

  /** @test */
  public function empty_fields_returns_default_of_unknown() {
    $user = ReqresUser::createOrUpdate(1);

    // Verify the entity was created with the correct values.
    $this->assertEquals('Unknown', $user->getFirstName());
    $this->assertEquals('Unknown', $user->getLastName());
    $this->assertEquals('Unknown', $user->getEmailAddress());
  }

  /** @test */
  public function base_field_definition_contains_expected_fields() {
    $entityType = $this->container->get('entity_type.manager')
      ->getStorage('reqres_user')
      ->getEntityType();
    $baseFields = ReqresUser::baseFieldDefinitions($entityType);

    $this->assertIsArray($baseFields);
    $this->assertArrayHasKey('email', $baseFields);
    $this->assertArrayHasKey('first_name', $baseFields);
    $this->assertArrayHasKey('last_name', $baseFields);
  }

}

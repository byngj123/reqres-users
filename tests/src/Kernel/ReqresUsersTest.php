<?php

namespace Drupal\Tests\reqres_users\Kernel;

use Exception;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\KernelTests\KernelTestBase;
use Drupal\reqres_users\ReqresUsers;

/**
 * Test description.
 *
 * @group reqres_users
 */
class ReqresUsersTest extends KernelTestBase {

  public EntityTypeManager $entityTypeManager;
  public ReqresUsers $service;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['reqres_users', 'dblog'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('reqres_user');
    $this->installSchema('dblog', ['watchdog']);

    $this->entityTypeManager = $this->container->get('entity_type.manager');

    $this->service = \Mockery::mock(ReqresUsers::class, [
      $this->container->get('http_client'),
      $this->container->get('logger.factory'),
      $this->container->get('entity_type.manager')
    ])
      ->shouldAllowMockingProtectedMethods()
      ->makePartial();
    $this->service->shouldReceive('getApiResponse')
      ->andReturn((object) [
        'total_pages' => 1,
        'data' => [
          (object) [
            'id' => 1,
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => 'john.doe@example.com'
          ]
        ]
      ]);
  }

  /** @test */
  public function can_sync_users() {
    $userStorage = $this->entityTypeManager->getStorage('reqres_user');
    $existingUsers = $userStorage->loadMultiple();

    $this->assertCount(0, $existingUsers);

    $this->service->syncUsers();

    $newUsers = $userStorage->loadMultiple();

    $this->assertCount(1, $newUsers);
  }

  /** @test */
  public function user_sync_deletes_removed_users() {
    $userStorage = $this->entityTypeManager->getStorage('reqres_user');

    $userStorage->create(['id' => 12])->save();
    $userStorage->create(['id' => 21])->save();
    $existingUsers = $userStorage->loadMultiple();

    $this->assertCount(2, $existingUsers);

    $this->service->syncUsers();

    $newUsers = $userStorage->loadMultiple();

    $this->assertCount(1, $newUsers);
    $this->assertEquals(1, reset($newUsers)->id());
  }

  /** @test */
  public function bad_api_response_doesnt_create_users() {
    $service = \Mockery::mock(ReqresUsers::class, [
      $this->container->get('http_client'),
      $this->container->get('logger.factory'),
      $this->container->get('entity_type.manager')
    ])
      ->shouldAllowMockingProtectedMethods()
      ->makePartial();
    $service->shouldReceive('getApiResponse')
      ->andThrows(new Exception('Unable to access API'));

    $userStorage = $this->entityTypeManager->getStorage('reqres_user');
    $existingUsers = $userStorage->loadMultiple();
    $logMessages = $this->getWatchdogMessages('Reqres User Sync');

    $this->assertCount(0, $existingUsers);
    $this->assertCount(0, $logMessages);

    $service->syncUsers();

    $newUsers = $userStorage->loadMultiple();
    $logMessages = $this->getWatchdogMessages('Reqres User Sync');

    $this->assertCount(0, $newUsers);
    $this->assertCount(1, $logMessages);
    $this->assertEquals('Unable to access API', $logMessages[0]->message);
  }

  protected function getWatchdogMessages($type) {
    $connection = $this->container->get('database');
    $query = $connection->select('watchdog', 'w')
      ->fields('w')
      ->condition('type', $type)
      ->execute();

    return $query->fetchAll();
  }

}

<?php

namespace Drupal\Tests\reqres_users\Unit;

use Drupal\Core\Form\FormState;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\reqres_users\Entity\ReqresUser;
use Drupal\reqres_users\Plugin\Block\ReqresUsersBlock;
use Drupal\Tests\UnitTestCase;

/**
 * Test description.
 *
 * @group reqres_users
 */
class ReqresUsersBlockTest extends UnitTestCase {

  use StringTranslationTrait;

  protected ReqresUsersBlock $block;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $user1 = \Mockery::mock(ReqresUser::class);
    $user1->shouldReceive('id')->andReturn(1);
    $user1->shouldReceive('getFirstName')->andReturn('John');
    $user1->shouldReceive('getLastName')->andReturn('Doe');
    $user1->shouldReceive('getEmailAddress')->andReturn('j.doe@example.com');

    $user2 = \Mockery::mock(ReqresUser::class);
    $user2->shouldReceive('id')->andReturn(2);
    $user2->shouldReceive('getFirstName')->andReturn('Jane');
    $user2->shouldReceive('getLastName')->andReturn('Doe');
    $user2->shouldReceive('getEmailAddress')->andReturn('jane.doe@example.com');

    $this->block = \Mockery::mock(ReqresUsersBlock::class)
      ->shouldAllowMockingProtectedMethods()
      ->makePartial();
    $this->block->shouldReceive('t')->andReturnUsing(function ($value) {
      return $value;
    }
    );
    $this->block->shouldReceive('getUsers')->andReturn(
      [
        $user1,
        $user2,
      ]
    );
  }

  /** @test */
  public function config_form_contains_expected_fields() {
    $form = $this->block->blockForm([], new FormState());

    $this->assertIsArray($form);
    $this->assertArrayHasKey('users_per_page', $form);
    $this->assertArrayHasKey('first_name_label', $form);
    $this->assertArrayHasKey('last_name_label', $form);
    $this->assertArrayHasKey('email_label', $form);
  }

  /** @test */
  public function submitting_the_form_sets_the_configuration() {
    $formState = new FormState();
    $formState->setValues([
      'users_per_page' => 1,
      'first_name_label' => 'Forename',
      'last_name_label' => 'Surname',
      'email_label' => 'mail',
    ]);

    $this->block->blockSubmit([], $formState);

    $configuration = $this->block->getConfiguration();

    $this->assertArrayHasKey('users_per_page', $configuration);
    $this->assertEquals(1, $configuration['users_per_page']);

    $this->assertArrayHasKey('first_name_label', $configuration);
    $this->assertEquals('Forename', $configuration['first_name_label']);

    $this->assertArrayHasKey('last_name_label', $configuration);
    $this->assertEquals('Surname', $configuration['last_name_label']);

    $this->assertArrayHasKey('email_label', $configuration);
    $this->assertEquals('mail', $configuration['email_label']);
  }

  /** @test */
  public function block_build_returns_expected_form_render_array() {
    $block = $this->block->build();

    $this->assertArrayHasKey('list', $block);
    $this->assertEquals('table', $block['list']['#type']);
    $this->assertEquals(
      [
        ['data' => 'First Name'],
        ['data' => 'Last Name'],
        ['data' => 'Email Address'],
      ],
      $block['list']['#header']
    );
    $this->assertEquals(
      [
        [
          'data' => [
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => 'j.doe@example.com',
          ],
        ],
        [
          'data' => [
            'first_name' => 'Jane',
            'last_name' => 'Doe',
            'email' => 'jane.doe@example.com',
          ],
        ],
      ],
      $block['list']['#rows']
    );
    $this->assertEquals('No users found', $block['list']['#empty']);
    $this->assertArrayHasKey('pager', $block);
    $this->assertEquals('pager', $block['pager']['#type']);
  }

}
